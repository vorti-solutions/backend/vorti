using System.Text;
using FastEndpoints.Swagger;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SharedKernel.Utilities;

namespace SharedKernel.Configs;

public static class ServiceExtension
{
    public static IServiceCollection AddSharedServices(
        this IServiceCollection services,
        ConfigurationManager config,
        ILogger logger
    )
    {
        services.AddProblemDetails();
        AddSwaggerDoc(services);
        AddAuthentication(services, config);
        AddAuthorization(services);

        logger.LogInformation(
            "########## Shared Kernel services registered successfully ##########"
        );

        return services;
    }

    public static IServiceCollection AddSharedConfigOptions(
        this IServiceCollection services,
        ConfigurationManager config,
        ILogger logger
    )
    {
        services.Configure<SwaggerOptions>(config.GetRequiredSection(SwaggerOptions.Position));
        services.Configure<AuthOptions>(config.GetRequiredSection(AuthOptions.Position));
        logger.LogInformation(
            " ########## Shared configuration options registered successfully ##########"
        );
        return services;
    }

    private static IServiceCollection AddSwaggerDoc(IServiceCollection services)
    {
        var swaggerOpt = services
            .BuildServiceProvider()
            .GetRequiredService<IOptions<SwaggerOptions>>()
            .Value;

        services.SwaggerDocument(o =>
        {
            o.DocumentSettings = s =>
            {
                s.Title = swaggerOpt.Title; //TODO: Read from AppSettings
                s.Version = swaggerOpt.Version1?.Label;
            };
            o.ShortSchemaNames = true;
            o.ShowDeprecatedOps = true;
            o.MaxEndpointVersion = swaggerOpt.Version1?.Value ?? 1;
            o.AutoTagPathSegmentIndex = 0;
        });

        return services;
    }

    private static IServiceCollection AddAuthentication(
        this IServiceCollection services,
        ConfigurationManager config
    )
    {
        var authOptions = config.GetRequiredSection(AuthOptions.Position).Get<AuthOptions>()!;
        services
            .AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new()
                {
                    ValidateAudience = true,
                    ValidateIssuer = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = authOptions.ValidIssuer,
                    ValidAudience = authOptions.ValidAudience,
                    IssuerSigningKey = new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(authOptions.Secret)
                    ),
                    ClockSkew = TimeSpan.FromSeconds(30),
                };
            });

        return services;
    }

    private static IServiceCollection AddAuthorization(
        this IServiceCollection services
    )
    {
        services.AddAuthorizationBuilder()
            .AddPolicy(AppConstant.ADMIN, x =>
            {
                x.RequireAuthenticatedUser();
                x.AddRequirements(new RolesAuthorizationRequirement([AppConstant.ADMIN]));
            })
            .AddPolicy(AppConstant.VENDOR, x =>
            {
                x.RequireAuthenticatedUser();
                x.AddRequirements(new RolesAuthorizationRequirement([AppConstant.VENDOR]));
            })
            .AddPolicy(AppConstant.DRIVER, x =>
            {
                x.RequireAuthenticatedUser();
                x.AddRequirements(new RolesAuthorizationRequirement([AppConstant.DRIVER]));
            })
            .AddPolicy(AppConstant.CUSTOMER, x =>
            {
                x.RequireAuthenticatedUser();
                x.AddRequirements(new RolesAuthorizationRequirement([AppConstant.CUSTOMER]));
            })
            .AddPolicy(AppConstant.SUPER_ADMIN, x =>
            {
                x.RequireAuthenticatedUser();
                x.AddRequirements(new RolesAuthorizationRequirement([AppConstant.SUPER_ADMIN]));
            })
            .AddPolicy(AppConstant.DELIVERY_AGENT, x =>
            {
                x.RequireAuthenticatedUser();
                x.AddRequirements(new RolesAuthorizationRequirement([AppConstant.DELIVERY_AGENT]));
            });

        return services;
    }
}
