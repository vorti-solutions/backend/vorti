using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace SharedKernel.Configs;

public static class LogExtension
{
    public static ILoggingBuilder AddLogging(this ILoggingBuilder builder)
    {
        var simpleConsoleOpt = GetSimpleConsoleOptions();
        builder.AddSimpleConsole(simpleConsoleOpt);
        return builder;
    }

    public static Action<SimpleConsoleFormatterOptions> GetSimpleConsoleOptions() =>
        opt =>
        {
            opt.IncludeScopes = true;
            opt.TimestampFormat = "dd:MM:yyyy HH:mm:ss";
            opt.SingleLine = false;
            opt.ColorBehavior = LoggerColorBehavior.Enabled;
        };

    public static Action<ILoggingBuilder> GetLogBuilderOptions()
    {
        var simpleConsoleOpt = GetSimpleConsoleOptions();
        return opt =>
        {
            opt.AddSimpleConsole(simpleConsoleOpt);
        };
    }
}
