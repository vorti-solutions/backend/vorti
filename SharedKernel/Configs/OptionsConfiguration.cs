namespace SharedKernel.Configs;

public class SwaggerOptions
{
    public const string Position = "Swagger";
    public string Title { get; set; } = string.Empty;
    public Version? Version1 { get; set; }
}

public class Version
{
    public string Label { get; set; } = string.Empty;
    public int Value { get; set; }
}

public class AuthOptions
{
    public const string Position = "AccessTokenAuth";
    public int Expiration { get; set; }
    public required string Secret { get; set; }
    public required string ValidIssuer { get; set; }
    public required string ValidAudience { get; set; }
}