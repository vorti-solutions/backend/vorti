namespace SharedKernel.Infrastructures.Entities;

public class BaseEntity
{
    public Guid Id { get; private set; } = Guid.Empty;
    public bool IsDeleted { get; set; } = false;
    public uint Version { get; private set; }
}
