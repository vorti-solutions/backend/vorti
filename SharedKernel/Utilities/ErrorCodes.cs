﻿namespace SharedKernel.Utilities
{
    public enum ErrorCodes
    {
        [ResponseCodeDescriber(
            "E001",
            "Our service is currently unavailable. Please try again later."
        )]
        GeneralError,

        [ResponseCodeDescriber("E002", "Format or validation error.")]
        ValidationError,

        [ResponseCodeDescriber("E003", "Unable to update user record.")]
        UpdateError,

        [ResponseCodeDescriber("E004", "Unable to save record.")]
        SaveError,

        [ResponseCodeDescriber("E005", "Email Or Mobile number already exist.")]
        EmailOrMobileNumberExistError,

        [ResponseCodeDescriber("E006", "Unable to locate record.")]
        NotFoundError,

        [ResponseCodeDescriber("E008", "Request could not be processed.")]
        RequestNotProcessedError,
    }

    [AttributeUsage(AttributeTargets.Field, Inherited = false)]
    sealed class ResponseCodeDescriberAttribute(string code, string description) : Attribute
    {
        public string Code { get; } = code;
        public string Description { get; } = description;
    }

    public static class ResponseCodeExtension
    {
        public static string GetCode(this ErrorCodes responseCode)
        {
            var type = typeof(ErrorCodes);
            var property = type.GetField(responseCode.ToString());
            var attribute = (ResponseCodeDescriberAttribute[])
                property!.GetCustomAttributes(typeof(ResponseCodeDescriberAttribute), false);
            return attribute[0].Code;
        }

        public static string GetDescription(this ErrorCodes responseCode)
        {
            var type = typeof(ErrorCodes);
            var property = type.GetField(responseCode.ToString());
            var attribute = (ResponseCodeDescriberAttribute[])
                property!.GetCustomAttributes(typeof(ResponseCodeDescriberAttribute), false);
            return attribute[0].Description;
        }
    }
}
