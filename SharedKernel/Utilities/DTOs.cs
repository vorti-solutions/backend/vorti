﻿namespace SharedKernel.Utilities;

public record BaseResponse(int Status, string Instance);

public record ProblemResponse(
    int Status,
    string Instance,
    string ErrorCode,
    string Description,
    IList<string>? Reasons = default
) : BaseResponse(Status, Instance);

public record ServiceResponse
{
    public required int Status { get; set; }
    public string? Instance { get; set; }
    public string? RequestID { get; set; }
    public string? TraceId { get; set; }
    public object? Data { get; set; }
}
 