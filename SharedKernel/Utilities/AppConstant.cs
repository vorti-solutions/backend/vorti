namespace SharedKernel.Utilities;

public static class AppConstant
{
    public const string METADATA_STATUS_KEY = "Status";

    #region app roles
    public const string ADMIN = "Admin";
    public const string DRIVER = "Driver";
    public const string VENDOR = "Vendor";
    public const string CUSTOMER = "Customer";
    public const string SUPER_ADMIN = "SuperAdmin";
    public const string DELIVERY_AGENT = "DeliveryAgent";

    public const string RECORD_SUCCESSFULLY_DELETED = "Record successfully deleted.";
    #endregion
}