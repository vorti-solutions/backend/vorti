using ErrorOr;
using FastEndpoints;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Logging;
using SharedKernel.Utilities;
using ErrorCodes = SharedKernel.Utilities.ErrorCodes;

namespace SharedKernel.EndpointProcessors;

public sealed class ResponseHandler(
    ILogger<ResponseHandler> logger,
    IProblemDetailsService problemDetailsService
) : IGlobalPostProcessor
{
    private readonly ILogger<ResponseHandler> _logger = logger;
    private readonly IProblemDetailsService _problemDetailsService = problemDetailsService;

    public Task PostProcessAsync(IPostProcessorContext context, CancellationToken ct)
    {
        _ = context.HttpContext.ResponseStarted();

        if (context.HasValidationFailures) //checks for dto validation
        {
            // Validation response has already been written at ValidationErrorHandler
            return Task.CompletedTask;
        }

        if (context.HasExceptionOccurred) //Handle unexpected errors
        {
            _logger.LogError(
                context.ExceptionDispatchInfo!.SourceException,
                "##### REST exception caught! | REST Endpoint: {httpMethod}|{endpoint} #####",
                context.HttpContext.Request.Method,
                context.HttpContext.Request.Path
            );

            return WriteException(context, ct);
        }

        var errorOr = (ErrorOr<ServiceResponse>)context.Response!;

        if (errorOr.IsError) //Handle domain errors
        {
            return WriteDomainProblems(context, errorOr);
        }

        return WriteSuccessResponse(context, errorOr, ct); //Handle Success results
    }

    async Task WriteException(IPostProcessorContext context, CancellationToken ct)
    {
        context.HttpContext.Response.StatusCode = StatusCodes.Status500InternalServerError;
        var activity = context.HttpContext.Features.Get<IHttpActivityFeature>()?.Activity;

        var problemDetails = new Microsoft.AspNetCore.Mvc.ProblemDetails
        {
            Title = ErrorCodes.GeneralError.GetCode(),
            Detail = ErrorCodes.GeneralError.GetDescription(),
            Status = StatusCodes.Status500InternalServerError,
            Instance = $"{context.HttpContext.Request.Method} {context.HttpContext.Request.Path}",
            Extensions = new Dictionary<string, object?>
            {
                { "requestId", context.HttpContext.TraceIdentifier },
                { "traceId", activity?.Id },
                {
                    "errors",
                    new string[] { context.ExceptionDispatchInfo?.SourceException?.Message ?? "" }
                },
            },
        };

        await _problemDetailsService.WriteAsync(
            new ProblemDetailsContext
            {
                HttpContext = context.HttpContext,
                Exception = context.ExceptionDispatchInfo?.SourceException,
                ProblemDetails = problemDetails,
            }
        );
    }

    async Task WriteDomainProblems(
        IPostProcessorContext context,
        ErrorOr<ServiceResponse> errorOr
    )
    {
        var httpStatus = (int)errorOr.FirstError.Metadata!["Status"];

        context.HttpContext.Response.StatusCode = httpStatus switch
        {
            StatusCodes.Status409Conflict or StatusCodes.Status404NotFound => httpStatus,
            _ => StatusCodes.Status422UnprocessableEntity,
        };

        var activity = context.HttpContext.Features.Get<IHttpActivityFeature>()?.Activity;

        var problemDetails = new Microsoft.AspNetCore.Mvc.ProblemDetails
        {
            Title = errorOr.FirstError.Code,
            Detail = errorOr.FirstError.Description,
            Status = httpStatus,
            Instance = $"{context.HttpContext.Request.Method} {context.HttpContext.Request.Path}",
            Extensions = new Dictionary<string, object?>
            {
                { "requestId", context.HttpContext.TraceIdentifier },
                { "traceId", activity?.Id },
            },
        };

        await _problemDetailsService.WriteAsync(
            new ProblemDetailsContext
            {
                HttpContext = context.HttpContext,
                ProblemDetails = problemDetails,
            }
        );
    }

    static async Task WriteSuccessResponse(
        IPostProcessorContext context,
        ErrorOr<ServiceResponse> errorOr,
        CancellationToken ct
    )
    {
        var httpStatus = errorOr.Value.Status;

        context.HttpContext.Response.StatusCode = httpStatus switch
        {
            StatusCodes.Status201Created
            or StatusCodes.Status202Accepted
            or StatusCodes.Status204NoContent => httpStatus,
            _ => StatusCodes.Status200OK,
        };

        var activity = context.HttpContext.Features.Get<IHttpActivityFeature>()?.Activity;
        errorOr.Value.Instance =
            $"{context.HttpContext.Request.Method} {context.HttpContext.Request.Path}";
        errorOr.Value.RequestID = context.HttpContext.TraceIdentifier;
        errorOr.Value.TraceId = activity?.Id;
        await context.HttpContext.Response.WriteAsJsonAsync(errorOr.Value, ct);
    }
}
