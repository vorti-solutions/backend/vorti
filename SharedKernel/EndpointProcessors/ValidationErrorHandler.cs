using FastEndpoints;
using FluentValidation.Results;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using SharedKernel.Utilities;
using ErrorCodes = SharedKernel.Utilities.ErrorCodes;

namespace SharedKernel.EndpointProcessors;

public sealed class ValidationErrorHandler(IProblemDetailsService problemDetailsService)
    : IGlobalPreProcessor
{
    private readonly IProblemDetailsService _problemDetailsService = problemDetailsService;

    public async Task PreProcessAsync(IPreProcessorContext context, CancellationToken ct)
    {
        _ = context.HttpContext.ResponseStarted();

        if (context.HasValidationFailures)
        {
            await WriteValidationFailures(context.ValidationFailures, context.HttpContext);
        }
    }

    async Task WriteValidationFailures(
        IReadOnlyCollection<ValidationFailure> validationFailures,
        HttpContext httpContext
    )
    {
        httpContext.Response.StatusCode = StatusCodes.Status400BadRequest;
        List<string> errors = [];
        foreach (var validationFailure in validationFailures)
        {
            errors.Add(validationFailure.ErrorMessage);
        }

        var activity = httpContext.Features.Get<IHttpActivityFeature>()?.Activity;

        var problemDetails = new Microsoft.AspNetCore.Mvc.ProblemDetails
        {
            Title = ErrorCodes.ValidationError.GetCode(),
            Detail = ErrorCodes.ValidationError.GetDescription(),
            Status = StatusCodes.Status400BadRequest,
            Instance = $"{httpContext.Request.Method} {httpContext.Request.Path}",
            Extensions = new Dictionary<string, object?>
            {
                { "requestId", httpContext.TraceIdentifier },
                { "traceId", activity?.Id },
                { "errors", errors },
            },
        };

        await _problemDetailsService.TryWriteAsync(
            new ProblemDetailsContext { HttpContext = httpContext, ProblemDetails = problemDetails }
        );
    }
}
