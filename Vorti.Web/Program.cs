using ErrorOr;
using FastEndpoints;
using FastEndpoints.Swagger;
using Admin.Application.Common.Configs;
using SharedKernel.Configs;
using SharedKernel.EndpointProcessors;
using SharedKernel.Types;
using ServiceExtension = Admin.Application.Common.Configs.ServiceExtension;
using Vendor.Application.Commom.Configs;
using Product.Application.Common.Configs;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

builder.Logging.AddLogging();
var loggerFactory = LoggerFactory.Create(LogExtension.GetLogBuilderOptions());
ILogger logger = loggerFactory.CreateLogger<Program>();
logger.LogInformation("########## Logging Configured Successfully ##########");

builder.Services.AddSharedConfigOptions(builder.Configuration, logger);

// GraphQL setup
builder
    .Services.AddGraphQLServer()
    .ModifyRequestOptions(o => o.IncludeExceptionDetails = true)
    .AddQueryType<QueryType>()
    .AddMutationType<MutationType>()
    .AddAdminTypes(logger);

// REST setup
builder.Services.AddSharedServices(builder.Configuration, logger);
builder.Services.AddFastEndpoints(o =>
{
    o.Assemblies = [typeof(ServiceExtension).Assembly];
});

builder.Services.AddAdminModuleServices(builder.Configuration, logger);
builder.Services.AddVendorModuleServices(builder.Configuration, logger);
builder.Services.AddProductModuleServices(builder.Configuration, logger);
var app = builder.Build();

// Configure the HTTP request pipeline.

if (app.Environment.IsDevelopment()) { }

// Middlewares
app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapGraphQL();
app.UseFastEndpoints(c =>
    {
        c.Endpoints.ShortNames = true;
        c.Endpoints.RoutePrefix = "api";
        c.Endpoints.Configurator = ep =>
        {
            if (ep.ResDtoType.IsAssignableTo(typeof(IErrorOr)))
            {
                ep.DontAutoSendResponse();
                ep.PreProcessor<ValidationErrorHandler>(Order.Before);
                ep.PostProcessor<ResponseHandler>(Order.After);
                ep.Description(b =>
                    b.ClearDefaultProduces()
                        .Produces(StatusCodes.Status200OK, ep.ResDtoType.GetGenericArguments()[0])
                        .ProducesProblemDetails()
                );
            }
        };
        c.Versioning.DefaultVersion = 1;
        c.Versioning.Prefix = "v";
        c.Versioning.PrependToRoute = true;
    })
    .UseSwaggerGen(
        _ => { },
        ui =>
        {
            ui.ConfigureDefaults();
        }
    );
app.UseStatusCodePages(); //default problem details display
app.Run();