﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Product.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product.Application.Common.Configs
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddProductModuleConfigOptions(
            this IServiceCollection services,
            ConfigurationManager config,
            ILogger logger
        )
        {
            logger.LogInformation(
                " ########## Product module configuration options registered sucessfully ##########"
            );

            return services;
        }

        public static IServiceCollection AddProductModuleServices(
            this IServiceCollection services,
            ConfigurationManager configuration,
            ILogger logger
        )
        {
            var connectionString = configuration.GetConnectionString("SQLConnection");
            logger.LogInformation("connectionString: {c}", connectionString);
            services.AddDbContextPool<ProductContext>(opts =>
                opts.UseNpgsql(
                    connectionString,
                    opts =>
                    {
                        opts.EnableRetryOnFailure(5);
                        opts.MigrationsAssembly(typeof(ServiceExtension).Assembly.FullName);
                    }
                )
            );

            logger.LogInformation(
                "########## Product module services registered successfully ##########"
            );

            return services;
        }
    }
}