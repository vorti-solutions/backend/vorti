﻿using SharedKernel.Infrastructures.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product.Infrastructure.Data.Entities
{
    internal class SubCategory : AuditableEntity
    {
        public string? Name { get; set; }
        public Guid CategoryId { get; set; }
        public bool IsActive { get; set; } = true;
    }
}