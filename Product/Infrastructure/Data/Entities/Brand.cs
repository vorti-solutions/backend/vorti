﻿using SharedKernel.Infrastructures.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product.Infrastructure.Data.Entities
{
    internal class Brand : AuditableEntity
    {
        public string? Name { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
