﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product.Infrastructure.Data
{
    internal class ProductContext : DbContext
    {
        public const string Schema = "Product";

        public ProductContext(DbContextOptions<ProductContext> options)
            : base(options) { }
    }
}
