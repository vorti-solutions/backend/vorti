﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vendor.Infrastructure.Data
{
    internal class VendorContext : DbContext
    {
        public const string Schema = "Vendor";

        public VendorContext(DbContextOptions<VendorContext> options)
            : base(options) { }
    }
}
