﻿using SharedKernel.Infrastructures.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vendor.Application.Commom.Enumeration;

namespace Vendor.Infrastructure.Data.Entities
{
    internal class DbVendor : AuditableEntity
    {
        public string? Name { get; set; }
        public string? RegNumber { get; set; }
        public string? Email { get; set; }
        public VendorType? VendorType { get; set; }
        public string? MobilePhone { get; set; }
        public string? AccountName { get; set; }
        public string? AccountNumber { get; set; }
        public string? BankName { get; set; }
        public string? Logo { get; set; }
        public double MarkUpRate { get; set; }
        public int SalesVolume { get; set; }
        public double MarkDownRate { get; set; }
        public string? Slug { get; set; }//to be reviewed later
        public string? LandMark { get; set; }
        public string? City { get; set; }
        public string? State { get; set; }
        public string? Country { get; set; }
        public string? CountryCode { get; set; }
        public string? CurrencyCode { get; set; }
        public int Tier { get; set; }
        public Guid TierHistoryId { get; set; }
        public Guid VendorCategoryId { get; set; } //M:M
        public Guid VendorSubCategoryId { get; set; }//M:M
        public string? RegulatoryBody { get; set; }
        public string? RegulatoryBodyCert { get; set; }
        public bool IsActive { get; set; }
        public string? TenanId { get; set; }
    }
}