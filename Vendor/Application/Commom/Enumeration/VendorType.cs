﻿using SharedKernel.Application.Common;

namespace Vendor.Application.Commom.Enumeration
{
    internal class VendorType : BaseEnumeration
    {
        public static readonly VendorType Distributor = new(1, nameof(Distributor));
        public static readonly VendorType Producer = new(2, nameof(Producer));
        public static readonly VendorType Manufacturer = new(3, nameof(Manufacturer));

        protected VendorType() { }

        protected VendorType(int value, string displayName) : base(value, displayName) { }
    }
}