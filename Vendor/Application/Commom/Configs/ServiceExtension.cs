﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vendor.Infrastructure.Data;

namespace Vendor.Application.Commom.Configs
{
    public static class ServiceExtension
    {
        public static IServiceCollection AddVendorModuleConfigOptions(
            this IServiceCollection services,
            ConfigurationManager config,
            ILogger logger
        )
        {
            logger.LogInformation(
                " ########## Vendor module configuration options registered sucessfully ##########"
            );

            return services;
        }

        public static IServiceCollection AddVendorModuleServices(
            this IServiceCollection services,
            ConfigurationManager configuration,
            ILogger logger
        )
        {
            var connectionString = configuration.GetConnectionString("SQLConnection");
            logger.LogInformation("connectionString: {c}", connectionString);
            services.AddDbContextPool<VendorContext>(opts =>
                opts.UseNpgsql(
                    connectionString,
                    opts =>
                    {
                        opts.EnableRetryOnFailure(5);
                        opts.MigrationsAssembly(typeof(ServiceExtension).Assembly.FullName);
                    }
                )
            );

            logger.LogInformation(
                "########## Vendor module services registered successfully ##########"
            );

            return services;
        }
    }
}