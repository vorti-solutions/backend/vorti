﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Admin.Application.Common.Enumerations;
using Admin.Application.Common.ValueObjects;
using SharedKernel.Infrastructures.Entities;
using SharedKernel.Application.Common;

namespace Admin.Infrastructure.Data.Entities
{
    class DbAdmin : AuditableEntity
    {
        public Guid UserId { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? ImageUrl { get; set; }
        public DateOnly DateOfBirth { get; set; }
        public Gender? Gender { get; set; }
        public Address? Address { get; set; }
        public string? CountryCode { get; set; }
    }

    class AdminEntityConfig : IEntityTypeConfiguration<DbAdmin>
    {
        public void Configure(EntityTypeBuilder<DbAdmin> builder)
        {
            builder.ToTable("Admin");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Version).IsRowVersion();
            builder.Property(x => x.FirstName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.LastName).HasMaxLength(50).IsRequired();
            builder.Property(x => x.Email).HasMaxLength(50).IsRequired();
            builder.Property(x => x.PhoneNumber).HasMaxLength(15).IsRequired();
            builder.HasIndex(x => x.UserId).IncludeProperties(x => new
            {
                x.Email,
                x.PhoneNumber
            }).IsUnique();
            builder.Property(x=>x.CountryCode).HasMaxLength(5).IsRequired();
            builder.Property(x => x.ImageUrl);
            builder.OwnsOne(x => x.Address);
            builder.Property(x => x.CreatedBy).HasMaxLength(50);
            builder.Property(x => x.UpdatedBy).HasMaxLength(50);
            builder.Property(x => x.DeletedBy).HasMaxLength(50);
            builder.Property(x => x.Gender).HasConversion(x => x!.DisplayName, x => BaseEnumeration.FromDisplayName<Gender>(x, true)!);
        }
    }
}