using Microsoft.EntityFrameworkCore;
using SharedKernel.Infrastructures.Entities;

namespace Admin.Infrastructure.Data;

class AdminContext : DbContext 
{
    public const string Schema = "Admin";

    public AdminContext(DbContextOptions<AdminContext> options)
        : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema(Schema);
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(AdminContext).Assembly);
    }

    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        HandleSaveChanges();
        return base.SaveChangesAsync(cancellationToken);
    }

    public override int SaveChanges()
    {
        HandleSaveChanges();
        return base.SaveChanges();
    }

    public void HandleSaveChanges()
    {
        var entries = ChangeTracker
            .Entries()
            .Where(e =>
                e.Entity is AuditableEntity
                && (e.State == EntityState.Added 
                || e.State == EntityState.Modified
                || e.State == EntityState.Deleted)
            );

        foreach (var entry in entries)
        {
            var entity = (AuditableEntity)entry.Entity;

            switch (entry.State)
            {
                case EntityState.Added:
                    entity.CreatedAt = DateTime.UtcNow;
                    entity.UpdatedAt = null;
                    entity.DeletedAt = null;
                    break;
                case EntityState.Modified:
                    entry.Property(nameof(AuditableEntity.CreatedAt)).IsModified = false;
                    entry.Property(nameof(AuditableEntity.DeletedAt)).IsModified = false;
                    entity.UpdatedAt = DateTime.UtcNow;
                    break;
                case EntityState.Deleted:
                    entry.Property(nameof(AuditableEntity.CreatedAt)).IsModified = false;
                    entry.Property(nameof(AuditableEntity.UpdatedAt)).IsModified = false;
                    entity.DeletedAt = DateTime.UtcNow;
                    break;
            }
        }
    }
}
