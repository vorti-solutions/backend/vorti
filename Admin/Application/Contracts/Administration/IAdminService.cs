﻿using Admin.Infrastructure.Data.Entities;
using ErrorOr;

namespace Admin.Application.Contracts.Administration
{
    internal interface IAdminService
    {
        Task<ErrorOr<DbAdmin>> AddAdminAsync(DbAdmin admin, CancellationToken ct);
        Task<ErrorOr<DbAdmin>> GetAdminAsync(Guid Id, CancellationToken ct);
        Task<ErrorOr<int>> DeleteAdminAsync(Guid Id, CancellationToken ct);
        Task<ErrorOr<IEnumerable<DbAdmin>>> GetAdminsAsync(CancellationToken ct);
    }
}
