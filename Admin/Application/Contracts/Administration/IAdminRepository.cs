﻿using Admin.Infrastructure.Data.Entities;
using System.Linq.Expressions;

namespace Admin.Application.Contracts.Administration
{
    internal interface IAdminRepository
    {
        Task<DbAdmin> AddAdminAsync(DbAdmin admin, CancellationToken ct);
        Task<bool> AnyAsync(Expression<Func<DbAdmin, bool>> predicate, CancellationToken ct);
        Task<DbAdmin?> GetAdminAsync(Guid Id, bool withTracking = false, CancellationToken ct = default);
        Task<int> UpdateAdminAsync(DbAdmin admin, CancellationToken ct);
        Task<IEnumerable<DbAdmin>> GetAdminsAsync(bool withTracking = false, CancellationToken ct = default);
    }
}
