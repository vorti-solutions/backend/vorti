﻿using System.Security.Claims;
using FastEndpoints;
using Mapster;
using Microsoft.AspNetCore.Http;
using Admin.Application.Common.Enumerations;
using Admin.Application.Common.ValueObjects;
using IMapper = MapsterMapper.IMapper;
using Admin.Infrastructure.Data.Entities;
using SharedKernel.Application.Common;

namespace Admin.Application.Features.Administration
{
    internal class AdminMapper : Mapper<AddAdminRequest, AdminResponse, DbAdmin>
    {
        /// <summary>
        /// FastEndpoint mapper
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public override DbAdmin ToEntity(AddAdminRequest r)
        {
            //scoped dependency resolving
            using var scope = CreateScope();
            var _httpContext = scope.Resolve<IHttpContextAccessor>();

            var gender = BaseEnumeration.FromValue<Gender>(r.Gender)!;

            var adminEntity = new DbAdmin
            {
                UserId = Guid.NewGuid(),//this should be replaced with UserId from gRPC call
                FirstName = r.FirstName,
                LastName = r.LastName,
                Email = r.Email,
                PhoneNumber = r.PhoneNumber,
                DateOfBirth = r.DateOfBirth,
                CountryCode = r.CountryCode,
                CreatedBy = _httpContext
                    .HttpContext!.User.FindFirst(x => x.Type == ClaimTypes.Email)!
                    .Value,
                Gender = BaseEnumeration.FromDisplayName<Gender>(gender.DisplayName)!,
                Address = Address.Create(r.AddressLine1!, r.City!, r.State!, r.Country!),
            };

            return adminEntity;
        }

        public override AdminResponse FromEntity(DbAdmin e)
        {
            using var scope = CreateScope();
            var mappers = scope.Resolve<IMapper>();
            var response = mappers.Map<AdminResponse>(e);
            response.AddressLine1 = e.Address!.AddressLine1;
            response.City = e.Address!.City;
            response.State = e.Address!.State;
            response.Country = e.Address!.Country;
            return response;
        }

        /// <summary>
        /// MapSter
        /// Map entities to response
        /// </summary>
        public static void Configure()
        {
            TypeAdapterConfig<DbAdmin, AdminResponse>
                .NewConfig()
                .Map(dest => dest.AddressLine1, src => src.Address!.AddressLine1)
                .Map(
                    dest => dest.Gender,
                    src => BaseEnumeration.FromValue<Gender>(src.Gender!.Value)!.DisplayName
                );
        }
    }

    internal class GetAdminMapper : ResponseMapper<AdminResponse, DbAdmin>
    {
        public override AdminResponse FromEntity(DbAdmin e)
        {
            using var scope = CreateScope();
            var mappers = scope.Resolve<IMapper>();

            var response = mappers.Map<AdminResponse>(e);
            response.AddressLine1 = e.Address!.AddressLine1;
            response.City = e.Address!.City;
            response.State = e.Address!.State;
            response.Country = e.Address!.Country;
            return response;
        }
    }

    internal class GetAdminsMapper : ResponseMapper<IEnumerable<AdminResponse>, IEnumerable<DbAdmin>>
    {
        public override IEnumerable<AdminResponse> FromEntity(IEnumerable<DbAdmin> e)
        {
            using var scope = CreateScope();
            var mappers = scope.Resolve<IMapper>();

            List<AdminResponse> responses = [];
            foreach (var admin in e)
            {
                var response = mappers.Map<AdminResponse>(admin);
                response.AddressLine1 = admin.Address!.AddressLine1;
                response.City = admin.Address!.City;
                response.State = admin.Address!.State;
                response.Country = admin.Address!.Country;
                responses.Add(response);
            }
            
            return responses;
        }
    }
}