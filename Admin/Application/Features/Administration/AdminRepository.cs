﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Admin.Application.Contracts.Administration;
using Admin.Infrastructure.Data;
using Admin.Infrastructure.Data.Entities;

namespace Admin.Application.Features.Administration
{
    internal class AdminRepository : IAdminRepository
    {
        private readonly AdminContext _context;
        private readonly IQueryable<DbAdmin> _query;

        public AdminRepository(AdminContext context)
        {
            _context = context;
            _query = _context.Set<DbAdmin>().AsQueryable();
        }

        public async Task<DbAdmin> AddAdminAsync(DbAdmin admin, CancellationToken ct)
        {
            await _context.AddAsync(admin, ct);
            await _context.SaveChangesAsync(ct);
            return admin;
        }

        public async Task<bool> AnyAsync(
            Expression<Func<DbAdmin, bool>> predicate,
            CancellationToken ct
        ) => await _context.Set<DbAdmin>().AnyAsync(predicate, ct);

        public async Task<DbAdmin?> GetAdminAsync(Guid Id, bool withTracking = false, CancellationToken ct = default)
        {
            if (withTracking)
            {
                _query.AsTracking();
            }
            else
            {
                _query.AsNoTracking();
            }

            return await _query.FirstOrDefaultAsync(x => x.Id == Id && x.IsDeleted == false, ct);
        }

        public async Task<int> UpdateAdminAsync(DbAdmin admin, CancellationToken ct)
        {
            _context.Update(admin);
            return await _context.SaveChangesAsync(ct);
        }

        public async Task<IEnumerable<DbAdmin>> GetAdminsAsync(bool withTracking = false, CancellationToken ct = default)
        {
            if (withTracking)
            {
                _query.AsTracking();
            }
            else
            {
                _query.AsNoTracking();
            }

            return await _query.Where(x => x.IsDeleted == false).ToListAsync(cancellationToken: ct);
        }
    }
}