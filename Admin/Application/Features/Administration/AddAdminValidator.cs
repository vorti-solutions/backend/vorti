﻿using FastEndpoints;
using FluentValidation;
using FluentValidation.Validators;

namespace Admin.Application.Features.Administration
{
    internal class AddAdminValidator : Validator<AddAdminRequest>
    {
        public AddAdminValidator()
        {
            RuleFor(x => x.FirstName).NotEmpty().MaximumLength(50);

            RuleFor(x => x.LastName).NotEmpty().MaximumLength(50);

            RuleFor(x => x.Email)
                .NotEmpty()
                .MaximumLength(50)
                .EmailAddress(EmailValidationMode.AspNetCoreCompatible);

            RuleFor(x => x.PhoneNumber).NotEmpty().MaximumLength(15);

            RuleFor(x => x.DateOfBirth)
                .NotEmpty()
                .Custom(
                    (x, ctx) =>
                    {
                        if (x.Year > DateTime.Now.Year)
                        {
                            ctx.AddFailure(
                                $"DateOfBirth cannot be greater than {DateTime.Now.Year}"
                            );
                        }
                    }
                );

            RuleFor(x => x.AddressLine1).NotEmpty().MaximumLength(250);

            RuleFor(x => x.City).NotEmpty().MaximumLength(40);

            RuleFor(x => x.State).NotEmpty().MaximumLength(40);
 
            RuleFor(x => x.Country).NotEmpty().MaximumLength(40);

            RuleFor(x => x.CountryCode).NotEmpty().MaximumLength(5);

            RuleFor(x => x.Gender).NotEmpty().InclusiveBetween(1, 3);
        }
    }
}
