﻿using ErrorOr;
using Microsoft.AspNetCore.Http;
using Admin.Application.Contracts.Administration;
using static SharedKernel.Utilities.ResponseCodeExtension;
using Error = ErrorOr.Error;
using ErrorCodes = SharedKernel.Utilities.ErrorCodes;
using System.Security.Claims;
using Admin.Infrastructure.Data.Entities;

namespace Admin.Application.Features.Administration
{
    internal class AdminService(
        IAdminRepository repository, 
        IHttpContextAccessor httpContext) : IAdminService
    {
        private readonly IAdminRepository _repository = repository;
        private readonly IHttpContextAccessor _httpContext = httpContext;

        public async Task<ErrorOr<DbAdmin>> AddAdminAsync(DbAdmin admin, CancellationToken ct)
        {
            var userExist = await _repository
                .AnyAsync(
                    x => x.Email!.Equals(admin.Email) || x.PhoneNumber!.Equals(admin.PhoneNumber),
                    ct
                )
                .ToErrorOr()
                .ThenAsync(async x => await x);

            var addAdmin = await userExist
                .FailIf(
                    x => x == true,
                    Error.Conflict(
                        ErrorCodes.EmailOrMobileNumberExistError.GetCode(),
                        ErrorCodes.EmailOrMobileNumberExistError.GetDescription(),
                        new Dictionary<string, object>
                        {
                            { "Status", StatusCodes.Status422UnprocessableEntity },
                        }
                    )
                )
                .ThenAsync(async x => await _repository.AddAdminAsync(admin, ct));

            if (addAdmin.IsError)
            {
                return addAdmin;
            }
            
            addAdmin.FailIf(
                x => x.Id.Equals(Guid.Empty),
                Error.Failure(ErrorCodes.SaveError.GetCode(), ErrorCodes.SaveError.GetDescription())
            );

            return addAdmin;
        }

        public async Task<ErrorOr<DbAdmin>> GetAdminAsync(Guid Id, CancellationToken ct)
        {
            var admin = await _repository
                .GetAdminAsync(Id, ct: ct);

            if (admin is null)
            {
                var error = Error.NotFound(
                    ErrorCodes.NotFoundError.GetCode(),
                    ErrorCodes.NotFoundError.GetDescription(),
                    new Dictionary<string, object>
                    {
                        {"Status", StatusCodes.Status422UnprocessableEntity }
                    }
                );

                return error;
            }

            
            return admin;
        }

        public async Task<ErrorOr<int>> DeleteAdminAsync(Guid Id, CancellationToken ct)
        {
            var admin = await _repository
                .GetAdminAsync(Id, ct: ct);

            if (admin is null)
            {
                var error = Error.Validation(
                    ErrorCodes.NotFoundError.GetCode(),
                    ErrorCodes.NotFoundError.GetDescription(),
                    new Dictionary<string, object>
                    {
                        {"Status", StatusCodes.Status422UnprocessableEntity }
                    }
                );

                return error;
            }

            var deleteResult = await admin
                .ToErrorOr()
                .ThenAsync(async x =>
                {
                    x.IsDeleted = !x.IsDeleted;
                    x.DeletedAt = DateTime.UtcNow; 
                    x.DeletedBy = _httpContext.HttpContext!.User
                    .FindFirst(e => e.Type == ClaimTypes.Email)!.Value;

                    var result = await _repository.UpdateAdminAsync(x, ct);
                    return result;
                });

            var result = deleteResult
                .FailIf(
                    x => x < 1,
                    Error.Failure(
                        ErrorCodes.RequestNotProcessedError.GetCode(),
                        ErrorCodes.RequestNotProcessedError.GetDescription(),
                        new Dictionary<string, object>
                        {
                            {"Status", StatusCodes.Status422UnprocessableEntity }
                        }
                    )
                );

            return result;
        }

        public async Task<ErrorOr<IEnumerable<DbAdmin>>> GetAdminsAsync(CancellationToken ct)
        {
            var admins = await _repository
                .GetAdminsAsync(ct: ct);

            if (!admins.Any())
            {
                var error = Error.Validation(
                    ErrorCodes.NotFoundError.GetCode(),
                    ErrorCodes.NotFoundError.GetDescription(),
                    new Dictionary<string, object>
                    {
                        {"Status", StatusCodes.Status422UnprocessableEntity }
                    }
                );

                return error;
            }

            return admins.ToErrorOr();
        }
    }
}