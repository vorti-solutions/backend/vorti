using HotChocolate.Execution.Configuration;
using Mapster;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Admin.APIs.Mutations;
using Admin.APIs.Queries;
using Admin.Application.Contracts.Administration;
using Admin.Application.Features.Administration;
using Admin.Infrastructure.Data;

namespace Admin.Application.Common.Configs;

public static class ServiceExtension
{
    public static IServiceCollection AddAdminModuleConfigOptions(
        this IServiceCollection services,
        ConfigurationManager config,
        ILogger logger
    )
    {
        logger.LogInformation(
            " ########## Admin module configuration options registered sucessfully ##########"
        );

        return services;
    }

    public static IServiceCollection AddAdminModuleServices(
        this IServiceCollection services,
        ConfigurationManager configuration,
        ILogger logger
    )
    {
        var connectionString = configuration.GetConnectionString("SQLConnection");
        logger.LogInformation("connectionString: {c}", connectionString);
        services.AddDbContextPool<AdminContext>(opts =>
            opts.UseNpgsql(
                connectionString,
                opts =>
                {
                    opts.EnableRetryOnFailure(5);
                    opts.MigrationsAssembly(typeof(ServiceExtension).Assembly.FullName);
                }
            )
        );

        services.AddMapster();
        services.AddScoped<IAdminRepository, AdminRepository>();
        services.AddScoped<IAdminService, AdminService>();

        logger.LogInformation(
            "########## Admin module services registered successfully ##########"
        );

        return services;
    }

    public static IRequestExecutorBuilder AddAdminTypes(
        this IRequestExecutorBuilder graphQlBuilder,
        ILogger logger
    )
    {
        graphQlBuilder
            .AddType<WeatherForecastType>()
            .AddTypeExtension<WeatherForecastQueryTypeExtension>()
            .AddType<AddWeatherInputType>()
            .AddTypeExtension<AddWeatherMutationTypeExtension>();

        logger.LogInformation(
            "########## Admin module graphQl types registered successfully ##########"
        );

        return graphQlBuilder;
    }
}