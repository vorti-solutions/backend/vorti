﻿using SharedKernel.Application.Common;

namespace Admin.Application.Common.ValueObjects
{
    internal class Address : BaseValueObject
    {
        private Address(string addressLine1, string city, string state, string country)
        {
            AddressLine1 = addressLine1;
            City = city;
            State = state;
            Country = country;
        }

        public string AddressLine1 { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Country { get; private set; }

        public static Address Create(string addressLine1, string city, string state, string country)
        {
            ArgumentException.ThrowIfNullOrWhiteSpace(addressLine1, nameof(addressLine1));
            ArgumentException.ThrowIfNullOrWhiteSpace(city, nameof(city));
            ArgumentException.ThrowIfNullOrWhiteSpace(state, nameof(state));
            return new Address(addressLine1, city, state, country);
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return AddressLine1;
            yield return City;
            yield return State;
            yield return Country;
        }
    }
}