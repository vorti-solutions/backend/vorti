﻿using SharedKernel.Application.Common;

namespace Admin.Application.Common.Enumerations
{
    internal class Gender : BaseEnumeration
    {
        public static readonly Gender Male = new(1, nameof(Male));
        public static readonly Gender Female = new(2, nameof(Female));
        public static readonly Gender Other = new(3, nameof(Other));

        protected Gender() { }

        protected Gender(int value, string displayName) : base(value, displayName) {}
    }
}