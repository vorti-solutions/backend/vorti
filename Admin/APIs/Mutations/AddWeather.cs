using System.Text.Json;
using Admin.APIs.Queries;
using Microsoft.Extensions.Logging;
using SharedKernel.Types;

namespace Admin.APIs.Mutations;

public class AddWeatherMutationTypeExtension : ObjectTypeExtension<Mutation>
{
    protected override void Configure(IObjectTypeDescriptor<Mutation> descriptor)
    {
        descriptor
            .Field("AddWeather")
            .Type<NonNullType<AddWeatherPayloadType>>()
            .Argument("input", x => x.Type<AddWeatherInputType>())
            .Resolve(
                (ctx) =>
                {
                    ILogger<AddWeatherMutationTypeExtension> logger = ctx.Service<ILogger<AddWeatherMutationTypeExtension>>();
                    var input = ctx.ArgumentValue<AddWeatherInput>("input");
                    var result = Weather.AddWeather(input.weather);
                    var allWeather = Weather.GetAllWeather();
                    logger.LogInformation("All Weather: {allWeather}", JsonSerializer.Serialize(allWeather));
                    return new AddWeatherPayload(result);
                }
            );
    }
}

public class AddWeatherInputType : InputObjectType<AddWeatherInput>
{
    protected override void Configure(IInputObjectTypeDescriptor<AddWeatherInput> descriptor)
    {
        descriptor
            .Field(x => x.weather)
            .Type<NonNullType<StringType>>()
            .Description("The name of the weather");
    }
}

public class AddWeatherPayloadType : ObjectType<AddWeatherPayload>
{
    protected override void Configure(IObjectTypeDescriptor<AddWeatherPayload> descriptor) { }
}

public record AddWeatherInput(string weather);

public record AddWeatherPayload(string weather);
