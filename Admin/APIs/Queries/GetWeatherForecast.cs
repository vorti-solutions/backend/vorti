using HotChocolate.Types;
using SharedKernel.Types;

namespace Admin.APIs.Queries; 

public class WeatherForecastQueryTypeExtension : ObjectTypeExtension<Query>
{
    protected override void Configure(IObjectTypeDescriptor<Query> descriptor)
    {
        descriptor
            .Field("WeatherForeCast")
            .Type<ListType<WeatherForecastType>>()
            .Resolve(ctx =>
            {
                var newSummaries = Weather.GetAllWeather();
                var forecast = Enumerable
                    .Range(1, 5)
                    .Select(index => new WeatherForecast(
                        DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                        Random.Shared.Next(-20, 55),
                        newSummaries[Random.Shared.Next(newSummaries.Count())]
                    ))
                    .ToList();
                return forecast;
            });
    }
}

public class WeatherForecastType : ObjectType<WeatherForecast>
{
    protected override void Configure(IObjectTypeDescriptor<WeatherForecast> descriptor) { }
}

public record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}

public static class Weather
{
    public static List<string> weathers =
    [
        "Freezing",
        "Bracing",
        "Chilly",
        "Cool",
        "Mild",
        "Warm",
        "Balmy",
        "Hot",
        "Sweltering",
        "Scorching"
    ];

    public static List<string> GetAllWeather()
    {
        return weathers;
    }

    public static string AddWeather(string weather)
    {
        if (!string.IsNullOrWhiteSpace(weather))
        {
            weathers.Add(weather);
        }
        return weather;
    }
}
