﻿using ErrorOr;
using FastEndpoints;
using Microsoft.AspNetCore.Http;
using Admin.APIs.EndpointGroups;
using Admin.Application.Contracts.Administration;
using Admin.Application.Features.Administration;
using SharedKernel.Utilities;

namespace Admin.APIs.Endpoints
{
    internal class AddAdmin(IAdminService service)
        : Endpoint<AddAdminRequest, ErrorOr<ServiceResponse>, AdminMapper>
    {
        private readonly IAdminService _service = service;

        public override void Configure()
        {
            Post("/");
            Policies(AppConstant.SUPER_ADMIN);
            Group<Administration>();
            Description(b => b.WithSummary("Endpoint to create admin user"));
            Summary(es =>
            {
                es.Summary = "Endpoint to create admin user";
            });
        }

        public override async Task<ErrorOr<ServiceResponse>> ExecuteAsync(
            AddAdminRequest req,
            CancellationToken ct
        )
        {
            var admin = Map.ToEntity(req);
            var result = await _service
                .AddAdminAsync(admin, ct)
                .MatchFirst(
                    x =>
                    {
                        var adminResponse = Map.FromEntity(x);
                        return new ServiceResponse
                        {
                            Status = StatusCodes.Status201Created,
                            Data = adminResponse,
                        }.ToErrorOr(); //replace empty string with uri to get admin
                    },
                    e => e
                );

            return result;
        }
    }
}
