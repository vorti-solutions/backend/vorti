﻿using Admin.APIs.EndpointGroups;
using Admin.Application.Contracts.Administration;
using ErrorOr;
using FastEndpoints;
using Microsoft.AspNetCore.Http;
using SharedKernel.Utilities;

namespace Admin.APIs.Endpoints
{
    internal class DeleteAdmin(IAdminService service) : EndpointWithoutRequest<ErrorOr<ServiceResponse>>
    {
        const string ID = "id";
        private readonly IAdminService _service = service;

        public override void Configure()
        {
            Delete("{id}/delete");
            Policies(AppConstant.SUPER_ADMIN);
            Group<Administration>();
            Description(b => b.WithSummary("Endpoint to delete admin user"));
            Summary(es =>
            {
                es.Summary = "Endpoint to delete admin user";
            });
        }

        public async override Task<ErrorOr<ServiceResponse>> ExecuteAsync(CancellationToken ct)
        {
            var result = await _service
                .DeleteAdminAsync(Route<Guid>(ID), ct)
                .MatchFirst(
                    x =>
                    {
                        return new ServiceResponse
                        {
                            Status = StatusCodes.Status200OK,
                            Data = AppConstant.RECORD_SUCCESSFULLY_DELETED
                        }.ToErrorOr();
                    },
                    e => e
                 );

            return result;
        }
    }
}