﻿using Admin.APIs.EndpointGroups;
using Admin.Application.Contracts.Administration;
using Admin.Application.Features.Administration;
using ErrorOr;
using FastEndpoints;
using Microsoft.AspNetCore.Http;
using SharedKernel.Utilities;

namespace Admin.APIs.Endpoints
{
    internal class GetAdmin(IAdminService service)
        : EndpointWithoutRequest<ErrorOr<ServiceResponse>, GetAdminMapper>
    {
        const string ID = "id";
        private readonly IAdminService _service = service;

        public override void Configure()
        {
            Get("{id}");
            Policies(AppConstant.SUPER_ADMIN);
            Group<Administration>();
            Description(b => b.WithSummary("Endpoint to fetch admin user"));
            Summary(es =>
            {
                es.Summary = "Endpoint to fetch admin user";
            });
        }

        public override async Task<ErrorOr<ServiceResponse>> ExecuteAsync(CancellationToken ct)
        {
            var result = await _service
                .GetAdminAsync(Route<Guid>(ID), ct)
                .MatchFirst(
                    x =>
                    {
                        var response = Map.FromEntity(x);
                        return new ServiceResponse
                        {
                            Status = StatusCodes.Status200OK,
                            Data = response
                        }.ToErrorOr();
                    },
                    e => e
                 );

            return result;
        }
    }
}