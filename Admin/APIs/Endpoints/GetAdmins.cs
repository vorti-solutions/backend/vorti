﻿using Admin.APIs.EndpointGroups;
using Admin.Application.Contracts.Administration;
using Admin.Application.Features.Administration;
using ErrorOr;
using FastEndpoints;
using Microsoft.AspNetCore.Http;
using SharedKernel.Utilities;

namespace Admin.APIs.Endpoints
{
    internal class GetAdmins(IAdminService service)
        : EndpointWithoutRequest<ErrorOr<ServiceResponse>, GetAdminsMapper>
    {
        private readonly IAdminService _service = service;

        public override void Configure()
        {
            Get("/");
            Policies(AppConstant.SUPER_ADMIN);
            Group<Administration>();
            Description(b => b.WithSummary("Endpoint to fetch admin users"));
            Summary(es =>
            {
                es.Summary = "Endpoint to fetch admin users";
            });
        }

        public override async Task<ErrorOr<ServiceResponse>> ExecuteAsync(CancellationToken ct)
        {
            var result = await _service
                .GetAdminsAsync(ct)
                .MatchFirst(
                    x =>
                    {
                        var response = Map.FromEntity(x);
                        return new ServiceResponse
                        {
                            Status = StatusCodes.Status200OK,
                            Data = response
                        }.ToErrorOr();
                    },
                    e => e
                 );

            return result;
        }
    }
}