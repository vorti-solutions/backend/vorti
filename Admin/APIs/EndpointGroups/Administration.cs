﻿using FastEndpoints;
using Microsoft.AspNetCore.Http;

namespace Admin.APIs.EndpointGroups
{
    internal class Administration : Group
    {
        public Administration()
        {
            Configure(
                "admin",
                ep =>
                {
                    ep.Description(x =>
                        x.Produces(StatusCodes.Status401Unauthorized).WithTags("administration")
                    );
                }
            );
        }
    }

    //internal class Profiles : SubGroup<Administration>
    //{
    //    public Profiles()
    //    {
    //        Configure(
    //            "profiles",
    //            ep =>
    //            {
    //                ep.Description(
    //                    x => x.Produces(StatusCodes.Status402PaymentRequired)
    //                    .WithTags("profiles"));
    //            });
    //    }
    //}
}